package co.com.comunidadjava.java8.core3.apitime;

public interface InterfaceTest {

	default void metodo() {
		System.out.println("Este es un metodo de la interface");
	}
	
	void metodoUnico();
	
	
	static String metodEstatico() {
		return "Este es un metodo estatico de una interfaz";
	}
	
}
