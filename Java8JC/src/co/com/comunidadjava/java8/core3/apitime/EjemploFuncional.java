package co.com.comunidadjava.java8.core3.apitime;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class EjemploFuncional {

	public static void main(String[] args) {

		List<Integer> listaEnteros = Arrays.asList(1,2,3,4,5);
		
		Integer reduce = listaEnteros.stream().reduce(0,(x,y)->x+y);
		System.out.println(reduce);
		
		List<Integer> listaEnterosSincronizados = Collections.synchronizedList(listaEnteros);
		Instant instant3 = Instant.now();
		reduce = listaEnterosSincronizados.stream().parallel().reduce(0, (x,y) ->x+y);
		Instant instant4 = Instant.now();
		long diferenciaEnMilisegundos2 = ChronoUnit.MILLIS.between(instant3, instant4);
		System.out.println("Suma de enteros en paralelo "+reduce+" diferenciaEnMilisegundos "+diferenciaEnMilisegundos2);		
		
	}

}
