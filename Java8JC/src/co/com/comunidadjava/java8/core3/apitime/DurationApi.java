package co.com.comunidadjava.java8.core3.apitime;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.temporal.Temporal;

public class DurationApi {

	public static void main(String[] args) {

		Period period = Period.ofDays(2);
		
		Period periodInMonths = Period.ofMonths(2);
		
		LocalDateTime localDateTime = LocalDateTime.now();
		System.out.println("Auemnto en dias: "+localDateTime.plus(period));
		
		System.out.println("Auemnto en meses: "+localDateTime.plus(periodInMonths));
		
		Duration duration = Duration.ofHours(6);
		System.out.println("duration "+duration);
		System.out.println("duracion 24 horas "+duration.multipliedBy(5));
		System.out.println("duracion mas 30 minutos "+duration.plusMinutes(30));		

		
		Temporal temporalDate = LocalDate.now();
		System.out.println("Temporal Date:  "+temporalDate);
		
		Temporal temporalLocalDateTime = LocalDateTime.now();
		System.out.println("Temporal2 Date:  "+temporalLocalDateTime);		
		
	}

}
