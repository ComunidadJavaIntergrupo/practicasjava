package co.com.comunidadjava.java8.core3.apitime;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Month;
import java.time.ZoneId;

public class ApiTime {

	public static void main(String[] args) {
		LocalDate localDate = LocalDate.of(1980, Month.AUGUST, 10);
		System.out.println(localDate);
		
		LocalDate localDate2 = LocalDate.now(ZoneId.of("Africa/Cairo"));
		
		System.out.println(localDate2);
		
		
		Instant instant = Instant.now();
		
		System.out.println(instant);
		
		String zonaColombiana = ZoneId.getAvailableZoneIds().stream().filter(zona->zona.contains("Bogota")).map(x->x).findFirst().get();
		ZoneId zonaLocal = ZoneId.of(zonaColombiana);		
		Instant instant2 = Instant.now(Clock.system(zonaLocal));
		System.out.println("instant2 "+instant2);		


	}

}
