package co.com.comunidadjava.java8.core3.apitime;

import java.time.chrono.Chronology;
import java.time.chrono.JapaneseDate;
import java.time.chrono.MinguoDate;
import java.time.format.DateTimeFormatter;
import java.util.Locale;

public class ChronologyApi {

	public static void main(String[] args) {
		
		Locale locale = Locale.forLanguageTag("th-TH-u-ca-buddhist");
		Chronology chronology = Chronology.ofLocale(locale);
		System.out.println(chronology);

		JapaneseDate japaneseDate = JapaneseDate.now();
		System.out.println("Fecha en japones:  "+japaneseDate);		
		
		MinguoDate minguoDate = MinguoDate.now();
		System.out.println("Fecha en minguo Date "+minguoDate);
		System.out.println("Fecha de minguo formateada "+minguoDate.format(DateTimeFormatter.ISO_LOCAL_DATE));		
		
	}

}
