package co.com.comunidadjava.java8.core3.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class CollectionExamples {

	private static Integer HEAD = 0;
	private static Integer TAIL = 1;
	private static Integer ROW_CERO = 0;
	
	
	/**
	 * @param args
	 * 2018
	 */
	public static void main(String[] args) {
		int[] listCoins = {1,0,0,1,0,0,0};
		ListCoins countHeadCoins = countHeadCoins(listCoins);
		
		System.out.println(countHeadCoins);
	}
	
	/**
	 * Method that let count the headers coins in an array of numbers
	 * @param listCoins
	 * @return
	 * 2018
	 */
	public static ListCoins countHeadCoins(int listCoins[]) {
		ListCoins listCoinsToResponse = new ListCoins();
		
		Objects.requireNonNull(listCoins);
		
		List<Integer> originalList = Arrays.stream(listCoins).boxed().collect(Collectors.toList());
		
		List<Integer> reverseList = new ArrayList<>();
		
		Iterator<Integer> iterator = originalList.iterator();
		
		Integer counterOfHeaders = 0;
		
		while(iterator.hasNext()) {
			Integer integerCoin = iterator.next();
			if(HEAD.equals(integerCoin)) {
				reverseList.add(TAIL);
			}else {
				reverseList.add(HEAD);
				listCoinsToResponse.addHeadPosition(ROW_CERO, counterOfHeaders++);
			}
		}
		
		listCoinsToResponse.setNumberOfHeadCoins(counterOfHeaders);
		
		listCoinsToResponse.setOriginalList(originalList);
		
		listCoinsToResponse.setReverseList(reverseList);
		
		return listCoinsToResponse;
	}

}
