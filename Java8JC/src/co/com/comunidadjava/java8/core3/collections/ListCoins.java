package co.com.comunidadjava.java8.core3.collections;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ListCoins {	
	
	private int numberOfHeadCoins;
	
	private List<Integer> originalList;
	
	private List<Integer> reverseList;
	
	Map<Integer,Integer> mapOfOriginalHeadPositions;

	
	public ListCoins() {
		mapOfOriginalHeadPositions = new HashMap<>();
	}
	
	public void addHeadPosition(Integer row, Integer column) {
		mapOfOriginalHeadPositions.put(row, column);
	}
	
	public int getNumberOfHeadCoins() {
		return numberOfHeadCoins;
	}

	public void setNumberOfHeadCoins(int numberOfHeadCoins) {
		this.numberOfHeadCoins = numberOfHeadCoins;
	}

	public List<Integer> getOriginalList() {
		return originalList;
	}

	public void setOriginalList(List<Integer> originalList) {
		this.originalList = originalList;
	}

	public List<Integer> getReverseList() {
		return reverseList;
	}

	public void setReverseList(List<Integer> reverseList) {
		this.reverseList = reverseList;
	}

	public Map<Integer, Integer> getMapOfOriginalHeadPositions() {
		return mapOfOriginalHeadPositions;
	}

	public void setMapOfOriginalHeadPositions(Map<Integer, Integer> mapOfOriginalHeadPositions) {
		this.mapOfOriginalHeadPositions = mapOfOriginalHeadPositions;
	}

	@Override
	public String toString() {
		return "ListCoins [numberOfHeadCoins=" + numberOfHeadCoins + ", originalList=" + originalList + ", reverseList="
				+ reverseList + ", mapOfOriginalHeadPositions=" + mapOfOriginalHeadPositions + "]";
	}
	
	
	
	
}
