package co.com.comunidadjava.java8.core3;

public abstract class Futbol implements Deporte {

	@Override
	public abstract String obtenerNombreDeporte();
}
