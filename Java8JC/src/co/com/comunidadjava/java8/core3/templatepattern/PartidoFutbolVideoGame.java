package co.com.comunidadjava.java8.core3.templatepattern;

public class PartidoFutbolVideoGame extends PartidoDeFutbol {

	
	
	@Override
	public void finalPartido() {
		System.out.println("Final del partido en video juego");
	}

	@Override
	public void calentamiento() {
		System.out.println("Calentamiento de los dedos del jugador");
	}

}
