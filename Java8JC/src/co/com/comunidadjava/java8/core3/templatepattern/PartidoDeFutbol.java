package co.com.comunidadjava.java8.core3.templatepattern;

public abstract class PartidoDeFutbol {

	
	/*
	 * Permite implementar el patron template para mostrar su potencial
	 * Las clases que sobre escriban esta clase pueden personalizar el comportamiento definido en este metodo
	 * */
	public final void jugarPartido() {
		calentamiento();
		inicioPartido();
		descansoEnMedioTiempo();
		finalPartido();
	}

	public abstract void finalPartido();

	public void descansoEnMedioTiempo() {
		System.out.println("Estamos en descanso");
	}

	public void inicioPartido() {
		System.out.println("Iniciando el partido");
	}

	public abstract void calentamiento();
	
}
