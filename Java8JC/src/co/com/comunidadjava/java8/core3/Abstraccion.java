package co.com.comunidadjava.java8.core3;

import java.util.ArrayList;
import java.util.List;

public class Abstraccion {

	public static void main(String[] args) {

		List<String> listaString = new ArrayList<>();
		
		Abstraccion abstraccion =  new Abstraccion();
		
		System.out.println(abstraccion.obtenerElDeporte().obtenerNombreDeporte());
		
	}
	
	public Deporte obtenerElDeporte() {
		Deporte deporte = new Tenis();		
		return deporte;
	}
	

}
