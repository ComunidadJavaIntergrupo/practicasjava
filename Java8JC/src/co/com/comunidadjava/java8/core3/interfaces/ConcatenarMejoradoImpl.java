package co.com.comunidadjava.java8.core3.interfaces;

public class ConcatenarMejoradoImpl implements Concatenar {
	
	private String cadena; 
	
	@Override
	public String ingresarCadena(String cadenaOriginal) {
		cadena = cadenaOriginal;
		return cadena;
	}
	
	@Override
	public String concatenar(String cadenaAConcatenar) {
		return cadena += cadenaAConcatenar;
	}

}
