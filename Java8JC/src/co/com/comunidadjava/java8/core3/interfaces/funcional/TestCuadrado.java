package co.com.comunidadjava.java8.core3.interfaces.funcional;

public class TestCuadrado {

	public static void main(String[] args) {
		
		Cuadrado  cuadradoDeUnNumero = (int numero) -> numero*numero;
		
		int calcualarCuadrado = cuadradoDeUnNumero.calcualarCuadrado(4);
		
		System.out.println(calcualarCuadrado);

	}

}
