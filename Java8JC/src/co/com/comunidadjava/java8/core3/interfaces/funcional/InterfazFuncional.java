package co.com.comunidadjava.java8.core3.interfaces.funcional;

@FunctionalInterface
public interface InterfazFuncional {
	void metodo();
}
