package co.com.comunidadjava.java8.core3.interfaces;

@FunctionalInterface
public interface Mayuscula {

	String convertirAMayuscula(String cadenaAConvertir);
}
