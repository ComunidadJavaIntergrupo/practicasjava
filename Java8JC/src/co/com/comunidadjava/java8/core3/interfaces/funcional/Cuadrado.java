package co.com.comunidadjava.java8.core3.interfaces.funcional;

@FunctionalInterface
public interface Cuadrado {

	int calcualarCuadrado(int numero);
	
}
