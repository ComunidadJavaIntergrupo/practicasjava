package co.com.comunidadjava.java8.core3.interfaces;

@FunctionalInterface
public interface Concatenar {

	String ingresarCadena(String cadenaOriginal);
	
	default String concatenar(String cadenaAConcatenar) {
		return cadenaAConcatenar+" + Cadena adicionada";
	}
}
