package co.com.comunidadjava.java8.blocks1;

import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author joseaylu
 * Esto es una hamburguesa
 */
public class Hamburger {
	
	private String typeHamburger = "type Mexican";
	
	 static boolean verdad = false;
	 byte b;
	 short corto;
	 int entero;
	 long largo;
	 float flotante;
	 double doble;
	 char cartacter;
	
	public Hamburger() {
		
	}
	
	public Hamburger(String typeHamburger) {
		this.typeHamburger = typeHamburger;
	}	
	
	
	/**
	 * @param args
	 * 2018
	 */
	public static void main(String[] args) {
		
		
		Hamburger hamburger = new Hamburger();
		Hamburger hamburger2 = new Hamburger();
		System.out.println(hamburger);
		hamburger = null;
		System.gc();
		
		System.out.println(hamburger2);
		
		Map<String, String> getenv = System.getenv();
		
		Set<String> keySet = getenv.keySet();
		
		Iterator<String> iterator = keySet.iterator();
		
		while(iterator.hasNext()) {
			String clave = iterator.next();
			
			String valor = getenv.get(clave);
			
			System.out.println("Clave:: "+clave+" valor:: "+valor);
		}
		
		
		Date fechActual = new Date();
		
		java.sql.Date fecha2 =new java.sql.Date(fechActual.getTime());
		System.out.println(fecha2);
		SimpleDateFormat dateFormat  = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss:z");
		
		System.out.println(dateFormat.format(fechActual));
		
	}

	/**
	 * @return the typeHamburger
	 */
	public String getTypeHamburger() {
		return typeHamburger;
	}
	
	
}

