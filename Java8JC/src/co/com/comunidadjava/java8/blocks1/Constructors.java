package co.com.comunidadjava.java8.blocks1;

public class Constructors {

	/**
	 * This is an empty constructor, just let the JVM to build an object from this class
	 */
	public Constructors() {
		System.out.println("This is a empty Contrctor");
	}

	public Constructors(String someParameter) {
		System.out.println("This is a Contrctor with one parameter, it value is "+someParameter);
	}
	
	public Constructors(String ...lotOfParameters) {
		System.out.println("This is a Contrctor with one parameter, it value is "+lotOfParameters);
	}
	
	public Constructors(int numberParam, String descriptionParam, Object whareverParam) {
		System.out.println("This is a Contrctor has three parameters: numberParam, descriptionParam adn whareverParam");
	}	
	
}
