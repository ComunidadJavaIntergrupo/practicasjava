/**
 * 
 */
package co.com.comunidadjava.java8.blocks1;

import java.util.Collection;
import java.util.Iterator;
import java.util.Map;

/**
 * @author josevicenteayalaluna
 * This is the body class
 */
public class Person {

	/*
	 * All this are members of the class
	 * */
	public String name;
	public long identification;	
	
	public String getName() {
		return name;
	}
	public long getIdentification() {
		return identification;
	}
	public void setName(String name) {
		this.name = name;
	}
	public void setIdentification(long identification) {
		this.identification = identification;
	}
	
	@Override
	protected void finalize() throws Throwable {
		System.out.println("Person.finalize()........");
		super.finalize();
	}
	
	/**
	 * @param args
	 * method to test the class
	 */
	public static void main(String[] args) {
		Person person = new Person();
		System.out.println(person);
		Map<String, String> getenv = System.getenv();
		Collection<String> values = getenv.keySet();
		Iterator<String> iterator = values.iterator();
		while(iterator.hasNext()) {
			String clave = iterator.next();
			System.out.println(clave+" --- "+getenv.get(clave));
			System.out.println("***********************");
		}
		person = null;
		System.gc();
		
		
	}
	
}
