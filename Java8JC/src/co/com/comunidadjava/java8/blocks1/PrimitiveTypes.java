package co.com.comunidadjava.java8.blocks1;

public class PrimitiveTypes {

	private boolean decisionVariable;
	private byte byteVariable;
	private short shortVariable;
	private int numberVariable;
	private long bigNumberVariable;
	private float floatvariable;
	private double doubleVariable;
	private char charVariable;
	
	public PrimitiveTypes(boolean decisionVariable, byte byteVariable, short shortVariable, int numberVariable,
			long bigNumberVariable, float floatvariable, double doubleVariable, char charVariable) {
		super();
		this.decisionVariable = decisionVariable;
		this.byteVariable = byteVariable;
		this.shortVariable = shortVariable;
		this.numberVariable = numberVariable;
		this.bigNumberVariable = bigNumberVariable;
		this.floatvariable = floatvariable;
		this.doubleVariable = doubleVariable;
		this.charVariable = charVariable;
	}
	
	
	
	
	/**
	 * @return only boolean, true in this case, but it can return false also
	 */
	public boolean isDecisionVariable() {
		return true;
	}




	/**
	 * @return a number no greater than 127
	 */
	public byte getByteVariable() {
		return 127;
	}




	/**
	 * @return a max 16 bit signed value minus one, maximum 32767
	 */
	public short getShortVariable() {
		return 32767;
	}




	/**
	 * @return a max 32 bit signed value minus one, maximum 2147483647
	 */
	public int getNumberVariable() {
		return 2147483647;
	}




	/**
	 * @return am max 64 bit signed value minus one.
	 */
	public long getBigNumberVariable() {
		return bigNumberVariable;
	}




	public float getFloatvariable() {
		return floatvariable;
	}




	public double getDoubleVariable() {
		return doubleVariable;
	}




	public char getCharVariable() {
		return charVariable;
	}




	@Override
	public String toString() {
		return "PrimitiveTypes [decisionVariable=" + decisionVariable + ", byteVariable=" + byteVariable
				+ ", shortVariable=" + shortVariable + ", numberVariable=" + numberVariable + ", bigNumberVariable="
				+ bigNumberVariable + ", floatvariable=" + floatvariable + ", doubleVariable=" + doubleVariable
				+ ", charVariable=" + charVariable + "]";
	}
}
