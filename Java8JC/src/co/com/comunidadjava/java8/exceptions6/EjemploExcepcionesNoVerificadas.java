package co.com.comunidadjava.java8.exceptions6;

public class EjemploExcepcionesNoVerificadas {

	public static void main(String[] args) {
		
		int cero = 0;
		int numeroDiferenteDeCero = 60;		
		try {
			System.out.println(numeroDiferenteDeCero/cero);
		}catch(Exception e) {
			e.printStackTrace();
		}
		System.out.println("despues de que ocurre la excepcion");
		
	}

}
