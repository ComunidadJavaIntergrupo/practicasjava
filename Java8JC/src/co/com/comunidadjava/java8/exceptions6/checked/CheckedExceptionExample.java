package co.com.comunidadjava.java8.exceptions6.checked;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class CheckedExceptionExample {

	public static void main(String[] args) {
		File file = new File("e:/Tmp/fileToFind.txt");
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			Stream<String> lines = bufferedReader.lines();
			Consumer<? super String> action = x->System.out.println(x);
			lines.forEach(action);
			bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
