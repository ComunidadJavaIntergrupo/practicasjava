package co.com.comunidadjava.java8.exceptions6;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

public class EjemploExcepcionesVerificadas {

	public static void main(String[] args) {		
		try {
			leerArchivo();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/*
	 * metodo que lee un archivo
	 * */
	private static void leerArchivo() throws FileNotFoundException, IOException {
		File file = new File("e:/Tmp/nuevoArchivo.txt");
		BufferedReader bufferedReader = null;
		try {
			FileReader fileReader = new FileReader(file);
			bufferedReader = new BufferedReader(fileReader);
			Stream<String> lines = bufferedReader.lines();
			Consumer<? super String> consumer = linea->System.out.println(linea);
			lines.forEach(consumer);
			
		}finally {
			if(Objects.nonNull(bufferedReader)) {
				bufferedReader.close();
			}
		}
	}

}
