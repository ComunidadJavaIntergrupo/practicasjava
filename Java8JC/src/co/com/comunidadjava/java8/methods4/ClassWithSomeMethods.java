package co.com.comunidadjava.java8.methods4;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ClassWithSomeMethods {

	public static void main(String[] args) {
		List<String> messageList = new ArrayList<>();
		messageList.add("this is the first message");
		messageList.add("say hi");
		messageList.add("response, hello");
		messageList.add("say good by");
		
		List<String> capitalCase = toCapitalCase(messageList);
		
		System.out.println(capitalCase);
	}
	
	public static List<String> toCapitalCase(List<String> messageList){
		return messageList.stream().map(String::toUpperCase).collect(Collectors.toList());
	}

}
