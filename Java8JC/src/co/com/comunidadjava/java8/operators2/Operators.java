package co.com.comunidadjava.java8.operators2;

public class Operators {

	public static final String CADENA = "CADENA";
	public static final String CADENA2 = "CADENA2";
	
	public static enum DAY{MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY}
	private static DAY dayOfweek = DAY.MONDAY; 
	public static void main(String[] args) {
		int number = 2;
		//double operation = number+number*--number;
		double operation = 3+ 2*-number/4;
		System.out.println(operation);
		
		System.out.println(9 / 3);
		System.out.println(9 % 3);
		System.out.println(!true);
		System.out.println(2 << 1);
		System.out.println(2 >> 2);
		System.out.println(2 >>> 2);
		
		switch(dayOfweek) {
		
		case MONDAY:{
			System.out.println(DAY.MONDAY);
		}
		case TUESDAY:{
			System.out.println(DAY.TUESDAY);
		}
		case WEDNESDAY:{
			System.out.println(DAY.WEDNESDAY);
		}
		case THURSDAY:{
			System.out.println(DAY.THURSDAY);
		}
		case FRIDAY:{
			System.out.println(DAY.FRIDAY);
		}
		case SATURDAY:{
			System.out.println(DAY.SATURDAY);
		}
		case SUNDAY:{
			System.out.println(DAY.SUNDAY);
		}
		default:
			System.out.println("Break");
			break;
		}
		
		
		switch (CADENA) {
		case (CADENA):{
			System.out.println(CADENA);
		}
		case (CADENA2):{
			System.out.println(CADENA2);
		}
		default:{
			System.out.println("DEFAULT");
		}
		}
	}

}
