package co.com.comunidadjava.java8.blocks1;

import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class PersonTest {
	private Person person;
	private static final Long IDENTIFICATION = 1234L;	
	private static final String NAME = "Jose Vicente";
	private static final Long IDENTIFICATION2 = 5678L;
	private static final String NAME2 = "Isabel";
	
	@BeforeEach
	void setUp() throws Exception {
		person = new Person();
		person.setIdentification(IDENTIFICATION);		
		person.setName(NAME);
	}

	@Test
	void testGetName() {
		assertTrue("The result must be Jose Vicente. ", NAME.equals(person.getName()));
	}

	@Test
	void testGetIdentification() {
		assertTrue("The identification must be 1234.", IDENTIFICATION.equals(person.getIdentification()));
	}

	@Test
	void testSetName() {
		person.setName(NAME2);
		assertTrue("The result must be Isabel. ", NAME2.equals(person.getName()));
	}

	@Test
	void testSetIdentification() {
		person.setIdentification(IDENTIFICATION2);
		assertTrue("The identification must be 5678.", IDENTIFICATION2.equals(person.getIdentification()));
	}

}
